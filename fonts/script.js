$(document).ready(function() {
	$('select').change(function(){
		var size = $('select option:selected').val();
		$('#preview').css({'font-size' : size + 'px'});
	});
	$('.fonts a').click(function(){
		var family = $(this).text();
		$(this).parent().parent().find('a').removeClass();
		$(this).addClass('active');
		$("#preview").css({'font-family' : family});
		return false;
	});
	$('.font-style a').click(function(){
		var style = $(this).attr('title');
		$(this).toggleClass('active');
		$('#preview').toggleClass(style);
		return false;
	});
	$('.font-style a.clear').click(function(){
		$('#preview').removeClass();
		$(this).parent().parent().find('a').removeClass('active');
		return false;
	});
	$('.text-align a').click(function(){
		var align = $(this).attr('title');
		$('#preview').css({'text-align' : align});
		return false;
	});
});