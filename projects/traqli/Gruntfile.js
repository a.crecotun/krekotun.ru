/*global module:false*/
module.exports = function(grunt) {
	'use strict';

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		stylus: {
			compile: {
				files: {
					'assets/styles/style.css' : 'dev/styles/style.styl'
				}
			}
		},
		watch: {
			css: {
				files: ['dev/styles/*.styl'],
				tasks: ['stylus'],
				options: {
					spawn: false,
				}
			}
		}
	});

	// load tasks
	grunt.loadNpmTasks('grunt-contrib-stylus');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// register tasks
	grunt.registerTask('default', ['stylus']);

};
