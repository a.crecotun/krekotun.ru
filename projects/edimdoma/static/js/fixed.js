$(document).ready(function(){
	var top = $('.menu-content .sidebar .menu-categories').offset().top - parseFloat($('.menu-content .sidebar .menu-categories').css('margin-top').replace(/auto/, 0));
	$(window).scroll(function (event) {
	  var y = $(this).scrollTop();
	  if (y >= top) {
		$('.menu-content .sidebar .menu-categories').addClass('fixed');
	  } else {
		$('.menu-content .sidebar .menu-categories').removeClass('fixed');
	  }
	});
});
